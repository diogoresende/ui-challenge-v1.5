(function () {
    'use strict';

    angular.module('GeoLocationApp', [])

        .controller('GeolocationController', function ($scope, $http) {
            var self = $scope;
            $scope.getLocation = function (url) {
                self.urllocation = {};
                $http.get(`http://freegeoip.net/json/${url}`).then(function (response) {
                    self.urllocation.ip = response.data.ip;
                    self.urllocation.time_zone = response.data.time_zone;
                    self.urllocation.city = response.data.city;
                    self.urllocation.country_code = response.data.country_code;
                    self.urllocation.country_name = response.data.country_name;
                    self.urllocation.region_code = response.data.region_code;
                    self.urllocation.latitude = response.data.latitude;
                    self.urllocation.longitude = response.data.longitude;
                    self.urllocation.longitude = response.data.longitude;
                    self.urllocation.metro_code = response.data.metro_code;
                    self.urllocation.zip_code = response.data.zip_code;
                }, invalidUrl);

                function invalidUrl() {
                    self.urllocation = {};
                    alert("invalid URL!");
                }
            };

            $scope.getMyLocation = function () {
                self.mylocation = {};
                $http.get('http://freegeoip.net/json/').then(function (json) {
                    self.mylocation.myip = json.data.ip;
                    self.mylocation.city = json.data.city;
                    self.mylocation.country_code = json.data.country_code;
                    self.mylocation.country_name = json.data.country_name;
                    self.mylocation.latitude = json.data.latitude;
                    self.mylocation.longitude = json.data.longitude;
                    self.mylocation.metro_code = json.data.metro_code;
                    self.mylocation.region_code = json.data.region_code;
                    self.mylocation.region_name = json.data.region_name;
                    self.mylocation.time_zone = json.data.time_zone;
                    self.mylocation.zip_code = json.data.zip_code;
                });
            };

            $scope.resetMyLocation = function () {
                self.mylocation = {};
            };
        });
})();