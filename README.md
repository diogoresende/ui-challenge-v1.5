=======================


What Is This?
-------------
This is a application made for ac-recruitment test.
I have decide to made this application in AngularJs.
The application was made using Visual Studio Code.

Description
-----------------------

It´s a basic AngularJs project, wich contains :

1. package.json file with the definition and dependencies.

2. app.js file with the module definition for initialize the app and register the modules on which it depends.

3. controller.js file with the functions used on view.

4 index.html the main view element of the application.



How To Install The Modules
--------------------------
The application was made using Visual Code Studio.
In terminal execute:

1. install node and git

2. open the project and on terminal execute the commands
	
	1. npm install
	2. npm install http-server -g

3. for run the project execute the command http-server on terminal and put the url http://localhost:8080 on browser

